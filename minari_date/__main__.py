from datetime import datetime
import sys

from dateutil.parser import parse

from . import MinariDateTime


if len(sys.argv) > 1:
    timestamp = parse(sys.argv[1]).replace(tzinfo=None)
else:
    timestamp = datetime.utcnow()

dt = MinariDateTime(timestamp)
print(str(dt))
